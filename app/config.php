<?php

$database = [
	'driver' => 'mysql',
	'host' => 'localhost',
	'port' => 3306,
	'user' => 'db_user',
	'name' => 'db_name',
	'password' => 'db_password'
];