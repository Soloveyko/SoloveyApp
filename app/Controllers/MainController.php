<?php


namespace Controllers;


use Exception;
use Solovey\Controller\Controller;
use Solovey\Controller\XController;

class MainController extends XController implements Controller
{
	
	function index()
	{
		success("home page");
	}
	
	function page(int $page)
	{
		try {
			$this->render('page', ['page' => $page]);
		} catch (Exception $e) {
			throw new $e;
		}
	}
	
}