<?php

use Controllers\MainController;
use SoloveyRouter\Router;

Router::pattern('email', '([-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+.[a-zA-Z]{2,4})');


Router::GET('/', [MainController::class]);

Router::GET('/page/{n+}', [MainController::class, 'page']);

Router::POST('/email/{email}', function ($email) {
	print "Email: $email";
});