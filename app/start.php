<?php

use Solovey\Solovey;

include_once 'config.php';
require_once 'router.php';

//Database::init($database);

Solovey::catch(function (Exception $e) {
	//handle exceptions
	error($e->getMessage(), is_numeric($e->getCode()) ? $e->getCode() : 500);
});

startApplication('app');